import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class DemoPanel extends JFrame implements ActionListener
{
    public static final int WIDTH = 1200;
    public static final int HEIGHT = 400;
    private JPanel originImgPanel, afterImgPanel, controlPanel;
    private JLabel originLabel, afterLabel;
    private ImageIcon originImg, afterImg;
    private JButton openFile, process;
    private JFileChooser fileChooser;
    private JRadioButton radioButton[];
    private ButtonGroup group;
    private String inputFile = "E:\\work\\homework\\EdgeDetection\\st.jpg";
    private int mode = 0;
    private Sobel sobel;


    public static void main(String[] args)
    {
        DemoPanel demoPanel = new DemoPanel();
        demoPanel.setVisible(true);
    }

    public DemoPanel()
    {
        super("Demo");
        setSize(WIDTH, HEIGHT);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        sobel = new Sobel();

        /* -------------- origin ---------------- */
        originImgPanel = new JPanel();
        originImgPanel.setLayout(new FlowLayout());
        originImgPanel.setBackground(Color.WHITE);

        // scale
        try
        {
            originImg = scale(500, -1, inputFile);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        originLabel = new JLabel(originImg);

        originImgPanel.add(originLabel);
        add(originImgPanel, BorderLayout.WEST);

        /* -------------- after ---------------- */
        afterImgPanel = new JPanel();
        afterImgPanel.setLayout(new FlowLayout());
        afterImgPanel.setBackground(Color.WHITE);

        //scale
//        try
//        {
//            afterImg = scale(600, -1, "E:\\work\\homework\\EdgeDetection\\fo.png");
//        } catch (IOException e)
//        {
//            e.printStackTrace();
//        }

        afterLabel = new JLabel(afterImg);

        afterImgPanel.add(afterLabel);
        add(afterImgPanel, BorderLayout.EAST);

        /* -------------- control ---------------- */
        controlPanel = new JPanel();
        controlPanel.setLayout(new FlowLayout());

        openFile = new JButton("Open");
        openFile.addActionListener(this);
        openFile.setBackground(Color.WHITE);
        openFile.setFont(new Font("Arial", Font.BOLD, 14));
        controlPanel.add(openFile);

        ButtonGroup group=new ButtonGroup();

        radioButton = new JRadioButton[3];
        radioButton[0] = new JRadioButton("Mix", true);
        radioButton[1] = new JRadioButton("Horizon", false);
        radioButton[2] = new JRadioButton("Vertical", false);

        for(int i=0; i < radioButton.length; i++)
        {
            controlPanel.add(radioButton[i]);
            radioButton[i].setFont(new Font("Arial", Font.BOLD, 14));
            radioButton[i].addActionListener(this);
            group.add(radioButton[i]);
        }

        process = new JButton("Process");
        process.addActionListener(this);
        process.setBackground(Color.WHITE);
        process.setFont(new Font("Arial", Font.BOLD, 14));

        controlPanel.add(process);
        add(controlPanel, BorderLayout.SOUTH);

    }

    private ImageIcon scale(int width, int height, String imgName) throws IOException
    {
        Image beforeScale, afterScale, read;
        read = ImageIO.read(new File(imgName));
        ImageIcon img = new ImageIcon(read);

        beforeScale = img.getImage();
        afterScale = beforeScale.getScaledInstance(width, height, Image.SCALE_SMOOTH);

        return new ImageIcon(afterScale);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getActionCommand().equals("Open"))
        {
            FileDialog fd = new FileDialog(this, "Choose a file", FileDialog.LOAD);
            fd.setDirectory("C:\\");
            fd.setFile("*.jpg;*.png");
            fd.setVisible(true);
            if (fd.getFile() == null)
                System.out.println("You cancelled the choice");
            else
            {
                inputFile = fd.getDirectory() + fd.getFile();
                System.out.println("You chose " + inputFile);
                try
                {
                    originImg = scale(500, -1, inputFile);
                    setSize(1050, originImg.getIconHeight() + 50);
                    afterLabel.setIcon(null);
                    originLabel.setIcon(originImg);

                } catch (IOException e1)
                {
                    e1.printStackTrace();
                }
            }
        }
        else if(e.getActionCommand().equals("Mix"))
        {
            mode = 0;
        }
        else if(e.getActionCommand().equals("Horizon"))
        {
            mode = 1;
        }
        else if(e.getActionCommand().equals("Vertical"))
        {
            mode = 2;
        }
        else if(e.getActionCommand().equals("Process"))
        {
            Image image = null;

            try
            {
                image = ImageIO.read(new File(inputFile));
                ImageIO.write(sobel.sobel((BufferedImage) image, mode), "jpg", new File("sobel.jpg"));
                String path = System.getProperty("user.dir");
                path = path.replace("\\", "\\\\");

                afterImg = scale(500, -1, path + "\\sobel.jpg");
                afterLabel.setIcon(afterImg);

            } catch (IOException e1)
            {
                e1.printStackTrace();
            }
        }
    }
}
