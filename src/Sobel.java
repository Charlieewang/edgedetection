import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Sobel
{
    int[] GxArray = {-1, 0, 1,
                     -2, 0, 2,
                     -1, 0, 1};

    int[] GyArray = {1, 2, 1,
                      0, 0, 0,
                      -1, -2, -1};

    public static void main(String[] args) throws IOException
    {
        String fileName = "D:\\develope\\java\\2018\\EdgeDetection\\edgedetection\\fo.png";
        File file = new File(fileName);
        BufferedImage image = ImageIO.read(file);
        Sobel sobel = new Sobel();

        sobel.changeToGray(image);

        File fileOut = new File("out.jpg");
        ImageIO.write(image, "jpg", fileOut);

        file = new File("out.jpg");
        image = ImageIO.read(file);
        ImageIO.write(sobel.sobel(image, 0), "jpg", new File("sobel.jpg"));


    }

    public void changeToGray(BufferedImage img)
    {
        for (int i = 0; i < img.getHeight(); i++)
        {
            for (int j = 0; j < img.getWidth(); j++)
            {
                Color c = new Color(img.getRGB(j, i));

                int red = (int) (c.getRed() * 0.2126);
                int green = (int) (c.getGreen() * 0.7152);
                int blue = (int) (c.getBlue() * 0.0593);

                Color newColor = new Color(
                        red + green + blue,
                        red + green + blue,
                        red + green + blue);

                img.setRGB(j, i, newColor.getRGB());
            }
        }
    }

    public BufferedImage sobel(BufferedImage img, int mode) // TODO change mask function?
    {
        int gx = 0, gy = 0, g = 0, maxGradient = -1;
        BufferedImage outImg = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        int[][] temp = new int[img.getWidth()][img.getHeight()];


        for(int i = 1; i < img.getWidth() - 1; i++)
        {
            for(int j = 1; j < img.getHeight() - 1; j++)
            {
                gx = sobelMatrixMultiply(img, GxArray, i, j);
                gy = sobelMatrixMultiply(img, GyArray, i, j);

                // TODO test here
                if(mode == 0)
                {
                    g = (int)Math.sqrt(Math.pow(gx, 2) + Math.pow(gy, 2));
                }
                else if(mode == 1)
                {
                    g = gx;
                }
                else if(mode == 2)
                {
                    g = gy;
                }

                // find max gradient
                if(maxGradient < g)
                {
                    maxGradient = g;
                }
//                outImg.setRGB(i, j, g);
                temp[i][j] = g;
            }
        }


        // normalization
        for(int i = 0; i < img.getWidth(); i++)
        {
            for(int j = 0; j < img.getHeight(); j++)
            {
                int edge = temp[i][j];
                double scale = 255.0 / maxGradient;
                edge = (int) (edge * scale);
                edge = 0xff000000 | (edge << 16) | (edge << 8) | edge;
                outImg.setRGB(i, j, edge);
            }
        }

        return outImg;
    }



    public int sobelMatrixMultiply(BufferedImage img, int[] intputMatrix, int i, int j)
    {
        return img.getRGB(i - 1, j - 1) * intputMatrix[0] +
                img.getRGB(i - 1, j) * intputMatrix[1] +
                img.getRGB(i - 1, j + 1) * intputMatrix[2] +
                img.getRGB(i, j - 1) * intputMatrix[3] +
                img.getRGB(i, j) * intputMatrix[4] +
                img.getRGB(i, j + 1) * intputMatrix[5] +
                img.getRGB(i + 1, j - 1) * intputMatrix[6] +
                img.getRGB(i + 1, j) * intputMatrix[7] +
                img.getRGB(i + 1, j + 1) * intputMatrix[8];
    }

}
